<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.greendesk.pl
 * @since             1.0.0
 * @package           Gd_Core_Functionality_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       GD Core Functionality Plugin
 * Plugin URI:        http://bitbucket.org/greendesk/gd-core-functionality-plugin
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Przemek Cichon
 * Author URI:        http://www.greendesk.pl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gd-core-functionality-plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gd-core-functionality-plugin-activator.php
 */
function activate_gd_core_functionality_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gd-core-functionality-plugin-activator.php';
	Gd_Core_Functionality_Plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gd-core-functionality-plugin-deactivator.php
 */
function deactivate_gd_core_functionality_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gd-core-functionality-plugin-deactivator.php';
	Gd_Core_Functionality_Plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gd_core_functionality_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_gd_core_functionality_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gd-core-functionality-plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gd_core_functionality_plugin() {

	$plugin = new Gd_Core_Functionality_Plugin();
	$plugin->run();

}
run_gd_core_functionality_plugin();
